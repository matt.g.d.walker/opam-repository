# runtime + build + doc dependencies
#
# This image includes
# - runtime dependencies (libraries linked at load time of the process)
# - non-opam build-dependencies (rust dependencies)
# - cache for opam build-dependencies
# - opam build-dependencies
# - opam test-dependencies (aclotest, crowbar, etc.)
# - python and python libraries for tests executed in python
# - nvm for javascript backend testing
#
# This image is intended for
# - building the CI docs of tezos


ARG BUILD_IMAGE

# hadolink ignore=DL3006
FROM ${BUILD_IMAGE}

COPY docs/poetry.lock poetry.lock
COPY docs/pyproject.toml pyproject.toml

RUN poetry config virtualenvs.in-project true && \
    poetry install
